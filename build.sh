#!/bin/bash

IMG="kruft/rabbitmq"
TAG='3.7-management-delayed_message-alpine'

docker build -t ${IMG}:${TAG} .
[ $? -eq 0 ] && docker push ${IMG}:${TAG}

FROM rabbitmq:3.7-management-alpine

ENV DME_VERSION 20171201
ENV DME_DOWNLOAD_URL https://dl.bintray.com/rabbitmq/community-plugins/3.7.x/rabbitmq_delayed_message_exchange/rabbitmq_delayed_message_exchange-${DME_VERSION}-3.7.x.zip

RUN wget -O /tmp/`basename ${DME_DOWNLOAD_URL}` ${DME_DOWNLOAD_URL} && \
    unzip -d /plugins/ /tmp/`basename ${DME_DOWNLOAD_URL}` && \
    rm /tmp/`basename ${DME_DOWNLOAD_URL}` && \
    rabbitmq-plugins enable --offline rabbitmq_delayed_message_exchange

